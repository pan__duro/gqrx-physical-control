/*

*/
#include <Arduino.h>
#include <inttypes.h>

#define FILTER 0
#define AUDIO 1
#define LNAGAIN 2
#define SQL 3

#define SPAN_FILTER 10000
#define MIN_FILTER 10
#define SPAN_AUDIO 86
#define MIN_AUDIO -80
#define SPAN_LNAGAIN 80
#define MIN_LNAGAIN -20
#define SPAN_FREQ 1000000
#define MIN_FREQ 1
#define FREQ_STEP 200
#define MIN_SQL -100

#define ALPHA 0.8

void setup()
{
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  digitalWrite(A0, HIGH);
  digitalWrite(A1, HIGH);
  digitalWrite(A2, HIGH);
  digitalWrite(A3, HIGH);
  digitalWrite(A4, HIGH); //pullup para los pines del encoder
  digitalWrite(A5, HIGH);
  PCICR |= (1 << PCIE1);                     //habilito la interrupcion del banco de pines 1
  PCMSK1 |= (1 << PCINT12) | (1 << PCINT13); //habilito la interrupcion solo en los pines del encoder
  PCIFR = 0;                                 //limpio los flags por si hay alguno seteado
  sei();                                     //habilita las interrupciones locales.
}
volatile unsigned long freq_new = 25000000;


ISR(BADISR_vect)
{

}

ISR(PCINT1_vect) //rutina interrupcion cambio de estado banco 1
{
  static int8_t A,B,A_t,B_t;

    B_t=digitalRead(A5);
    A_t=digitalRead(A4);
    if (A!=A_t) 
    {
      if(A^B == 1)
      {
        freq_new+=FREQ_STEP;
      }
      else
      {
        freq_new-=FREQ_STEP;
      }
    }
    else
    {
      if(A^B == 1)
      {
        freq_new-=FREQ_STEP;
      }
      else
      {
        freq_new+=FREQ_STEP;
      }
    }
    
  A=A_t;
  B=B_t;
  PCIFR = 0;
}

void loop()
{
  static float raw_lna_gain, raw_a_gain, raw_filter, raw_sql;
  int16_t f_width_new, a_gain_new, lna_gain_new, sql_level_new;
  static int16_t f_width, a_gain, lna_gain, sql_level;
  static unsigned long freq;

  /*Filter input signals*/
  raw_lna_gain = raw_lna_gain * ALPHA + analogRead(LNAGAIN) * (1 - ALPHA);
  raw_a_gain = raw_a_gain * ALPHA + analogRead(AUDIO) * (1 - ALPHA);
  raw_filter = raw_filter * ALPHA + analogRead(FILTER) * (1 - ALPHA);
  raw_sql = raw_sql * ALPHA + analogRead(SQL) * (1 - ALPHA);

  /*scaling  */
  f_width_new = MIN_FILTER + (SPAN_FILTER * raw_filter) / 1024;
  a_gain_new = 13 - (128 - raw_a_gain / 8);
  lna_gain_new = MIN_LNAGAIN + (raw_lna_gain * SPAN_LNAGAIN) / 1024;
  sql_level_new = (raw_sql * MIN_SQL) / 1024;

  /* Parámetros que tardan mucho en cambiar y laggean...*/
  //Serial.print("M AM ");
  //Serial.println(f_width);

  if (freq != freq_new)
  {
    freq = freq_new;
    Serial.print(" F ");
    Serial.println(freq);
    Serial.readStringUntil('\n'); //espero que gqrx devuelva el comando
  }

  if (a_gain != a_gain_new)
  {
    a_gain = a_gain_new;
    Serial.print(" L AUDIO_GAIN ");
    Serial.println(a_gain);
    Serial.readStringUntil('\n'); //espero que gqrx devuelva el comando
  }

  if (lna_gain != lna_gain_new)
  {
    lna_gain = lna_gain_new;
    Serial.print(" L LNA_GAIN ");
    Serial.println(lna_gain);
    Serial.readStringUntil('\n'); //espero que gqrx devuelva el comando
  }

  if (sql_level != sql_level_new) //mando solo si el parámetro cambió (para más velocidad )
  {
    sql_level = sql_level_new;
    Serial.print(" L SQL ");
    Serial.println(sql_level);
    Serial.readStringUntil('\n'); //espero que gqrx devuelva el comando
  }
  /* */
}
