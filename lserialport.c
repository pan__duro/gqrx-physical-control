/******************************************************************************

    Copyright (C) 2020 Matías Sebastián Ávalos (@tute_avalos)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <libserialport.h>

#define SERIALBUFFMAX   1024
#define TCPBUFFMAX      1024

struct sp_port *eserial = NULL;
int esock = 0;

/** @brief Verifica si hay algún error al intentar abrir o incializar el puerto
 *
 *  El programa termina con estado -1 si se encuentra un error.
 *
 */
void check_sp(const enum sp_return ret, const char *port_name);

/** @brief Cierra el puerto serie amablemente.
 * 
 *  Si se llegó a instanciar un puerto serie en esta función se cierra y liberan
 *  los recursos ocupados por el mismo.
 */
void close_serial(void);

/** @brief Ataja el Ctrl-C para cerrar los ports y terminar con 0
 */
void intHandler(int sig)
{
    close_serial();
    if (esock != 0)
        close(esock);
    puts("Adiós");
    exit(0);
}

int main(int argc, char *argv[])
{
    /* Catching Ctrl-C */
    struct sigaction act;
    act.sa_handler = intHandler;
    sigaction(SIGINT, &act, NULL);

    /* serial variables */
    char serial_buff[SERIALBUFFMAX];
    int serial_index = 0;

    /* TCP Socket variables */
    int sock;
    struct sockaddr_in serv_addrs;
    unsigned short serv_port;
    char *serv_ip;
    char tcp_buff[TCPBUFFMAX];
    int tcp_index = 0;

    /* Usage */
    if (argc != 5)
    {
        printf("Usage:\n\t%s <serial-device> <baudrate> <server-ip> <port>\n\n", argv[0]);
        printf("Example:\n\t%s /dev/ttyUSB0 9600 127.0.0.1 7356\n", argv[0]);
        exit(0);
    }

    /* Serial Init */
    struct sp_port *serial;
    check_sp(sp_get_port_by_name(argv[1], &serial), argv[1]);
    check_sp(sp_open(serial, SP_MODE_READ_WRITE), argv[1]);
    check_sp(sp_set_baudrate(serial, atoi(argv[2])), argv[1]);
    check_sp(sp_set_bits(serial, 8), argv[1]);
    check_sp(sp_set_parity(serial, SP_PARITY_NONE), argv[1]);
    check_sp(sp_set_stopbits(serial, 1), argv[1]);
    check_sp(sp_set_flowcontrol(serial, SP_FLOWCONTROL_NONE), argv[1]);
    eserial = serial;

    /* Socket Init */
    serv_ip = argv[3];
    serv_port = atoi(argv[4]);
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        fputs("No se pudo abrir el socket.", stderr);
        close_serial();
        exit(-1);
    }
    memset(&serv_addrs, 0, sizeof(serv_addrs));
    serv_addrs.sin_family = AF_INET;
    serv_addrs.sin_addr.s_addr = inet_addr(serv_ip);
    serv_addrs.sin_port = htons(serv_port);
    if (connect(sock, (struct sockaddr *)&serv_addrs, sizeof(serv_addrs)) < 0)
    {
        fputs("No se pudo conectar al servidor.", stderr);
        close_serial();
        exit(-1);
    }
    esock = sock;

    /* Indicaciones para el usuario */
    printf("\nSe ha establecido la conexión: [%s] <-> [%s:%s]", argv[1], argv[3], argv[4]);
    puts("\n\nPresione Ctrl+C para finalizar\n");

    while (1)
    {
        // Serial => TCP
        if (sp_input_waiting(serial) > 0)
        {
            char buff[SERIALBUFFMAX];
            int size = sp_blocking_read(serial, buff, SERIALBUFFMAX, 10);
            for (int i = 0; i < size; i++, serial_index++)
                serial_buff[serial_index] = buff[i];

            if (serial_buff[serial_index - 1] == '\n')
            {
                serial_buff[serial_index] = '\0';
                printf("[%s] : %s", argv[1], serial_buff);
                fflush_unlocked(stdout);
                if (send(sock, serial_buff, serial_index, 0) != serial_index)
                    fputs("Error de envío...", stderr);

                serial_index = 0;
            }
        }
        // TCP => Serial
        if ((tcp_index = recv(sock, tcp_buff, TCPBUFFMAX - 1, MSG_DONTWAIT)) > 0)
        {
            tcp_buff[tcp_index] = '\0';
            printf("[%s:%s] : %s", argv[3], argv[4], tcp_buff);
            fflush_unlocked(stdout);
            sp_blocking_write(serial, tcp_buff, tcp_index, 10);
        }
    }

    return 0;
}

void check_sp(const enum sp_return ret, const char *port_name)
{
    if (ret != SP_OK)
    {
        fprintf(stderr, "Eror al intentar abrir el puerto %s\n", port_name);
        exit(-1);
    }
}

void close_serial(void)
{
    puts("\nCerrando los puertos...");
    if (eserial != NULL)
    {
        sp_close(eserial);
        sp_free_port(eserial);
    }
}
